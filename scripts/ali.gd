extends AnimatedSprite2D

func _ready():
	get_sprite_frames().set_animation_speed("walk", 10.0)
	#set_process(true)
	set_process_input(true)

func _input(event):
	if Input.is_action_pressed("ui_right"):
		play("walk")
	elif Input.is_action_just_released("ui_right"):
		play("idle")
