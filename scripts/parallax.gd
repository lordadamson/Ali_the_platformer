extends ParallaxBackground

var right_pressed = false

func _ready():
	set_process_input(true)
	set_process(true)

func _process(delta):
	if right_pressed:
		get_node("skyLayer").move_local_x(-5*delta)
		get_node("mountainsLayer").move_local_x(-20*delta)
		get_node("groundLayer").move_local_x(-110*delta)
		if get_node("groundLayer").global_position.x < -1228.800049:
			get_node("groundLayer").global_position = Vector2(0, 0)
		if get_node("mountainsLayer").global_position.x < -1228.800049:
			get_node("mountainsLayer").global_position = Vector2(0, 0)
		if get_node("skyLayer").global_position.x < -1228.800049:
			get_node("skyLayer").global_position = Vector2(0, 0)
	

func _input(event):
	if Input.is_action_pressed("ui_right"):
		right_pressed = true
	elif Input.is_action_just_released("ui_right"):
		right_pressed = false
